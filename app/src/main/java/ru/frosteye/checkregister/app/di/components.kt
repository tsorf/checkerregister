package ru.frosteye.checkregister.app.di

import dagger.Component
import dagger.Subcomponent
import ru.frosteye.checkregister.execution.network.Api
import ru.frosteye.checkregister.presentation.view.impl.activity.BaseActivity
import ru.frosteye.checkregister.presentation.view.impl.activity.MainActivity
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface AppComponent {
    operator fun plus(module: PresenterModule): PresenterComponent

    fun api(): Api
}

@Subcomponent(modules = [PresenterModule::class])
@PresenterScope
interface PresenterComponent {
    fun inject(activity: BaseActivity)
    fun inject(activity: MainActivity)
}