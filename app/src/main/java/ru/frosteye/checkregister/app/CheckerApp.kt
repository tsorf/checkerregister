package ru.frosteye.checkregister.app

import android.app.Application
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import ru.frosteye.checkregister.R
import ru.frosteye.checkregister.app.di.AppComponent
import ru.frosteye.checkregister.app.di.AppModule
import ru.frosteye.checkregister.app.di.DaggerAppComponent
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class CheckerApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("regular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}