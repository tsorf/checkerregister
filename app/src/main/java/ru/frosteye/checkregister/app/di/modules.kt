package ru.frosteye.checkregister.app.di

import dagger.Module
import dagger.Provides
import ru.frosteye.checkregister.app.CheckerApp
import ru.frosteye.checkregister.execution.network.Api
import ru.frosteye.checkregister.execution.network.ApiClient
import ru.frosteye.checkregister.execution.network.RestClient
import ru.frosteye.checkregister.presentation.presenter.MainPresenter
import ru.frosteye.checkregister.presentation.presenter.MainPresenterImpl
import ru.frosteye.checkregister.presentation.view.impl.activity.BaseActivity
import ru.frosteye.checkregister.presentation.view.impl.fragment.BaseFragment
import ru.frosteye.ovsa.di.module.BaseAppModule
import ru.frosteye.ovsa.di.module.BasePresenterModule
import javax.inject.Singleton

@Module
class AppModule(app: CheckerApp) : BaseAppModule<CheckerApp>(app) {

    @Provides @Singleton fun apiClient(client: RestClient): ApiClient = client
    @Provides @Singleton fun api(client: ApiClient): Api = client.api
}

@Module
class PresenterModule : BasePresenterModule<BaseActivity, BaseFragment> {
    constructor(activity: BaseActivity?) : super(activity)
    constructor(fragment: BaseFragment?) : super(fragment)

    @Provides @PresenterScope fun main(impl: MainPresenterImpl): MainPresenter = impl
}