package ru.frosteye.checkregister.execution.network

import com.google.gson.annotations.SerializedName

data class DataResponse<T>(
        @SerializedName("data") val data: T
)