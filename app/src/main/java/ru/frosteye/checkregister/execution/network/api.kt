package ru.frosteye.checkregister.execution.network

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import org.json.JSONObject
import retrofit2.Call
import retrofit2.HttpException
import retrofit2.http.*
import ru.frosteye.checkregister.BuildConfig
import ru.frosteye.checkregister.data.model.Attendee
import ru.frosteye.checkregister.data.model.Shop
import ru.frosteye.ovsa.R
import ru.frosteye.ovsa.data.storage.ResourceHelper
import ru.frosteye.ovsa.execution.network.client.BaseRetrofitClient
import ru.frosteye.ovsa.execution.network.request.MultipartRequestParams
import ru.frosteye.ovsa.execution.network.request.RequestParams
import ru.frosteye.ovsa.execution.task.toFront
import javax.inject.Inject

interface Api {

    @GET("attendees/")
    fun getAttendees(@Query("query") query: String,
                     @Query("count") count: Int? = 10): Call<DataResponse<List<Attendee>>>

    @GET("shops/")
    fun getShops(@Query("query") query: String,
                 @Query("count") count: Int? = 10): Call<DataResponse<List<Shop>>>

    @GET("shops/byfn/")
    fun getShop(@Query("fn") fn: String): Observable<Shop>

    @GET("attendees/{id}/")
    fun getAttendee(@Path("id") id: Int): Observable<Attendee>

    @GET("checks/add/")
    fun addCheck(@QueryMap params: RequestParams): Observable<Attendee>

    @GET("checks/addwithattendee/")
    fun addCheckWithAttendee(@QueryMap params: RequestParams): Observable<Attendee>

    @POST("resetprint/{id}/")
    fun resetPrint(@Path("id") attendeeId: Int): Observable<Any>
}

interface ApiClient {
    var api: Api
}

fun <T : Any> Observable<T>.apiRequest(result: (T) -> Unit, error: (Throwable) -> Unit) : Disposable {
    return this
            .toFront()
            .subscribe({
                result.invoke(it)
            }, {
                try {
                    if (it is HttpException) {
                        val body = it.response().errorBody()?.string()
                        var o = JSONObject(body)
                        o = o.getJSONObject("error")
                        error.invoke(ru.frosteye.ovsa.execution.network.base.ApiException(
                                o.getString("message"), o.getInt("code")
                        ))
                    } else {
                        it.printStackTrace()
                        error.invoke(ru.frosteye.ovsa.execution.network.base.ApiException(
                                ResourceHelper.getString(R.string.default_connection_error), 0
                        ))
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    error.invoke(ru.frosteye.ovsa.execution.network.base.ApiException(
                            ResourceHelper.getString(R.string.default_connection_error), 0
                    ))
                }

            })
}

class RestClient @Inject constructor() : BaseRetrofitClient<Api>(baseUrl = BuildConfig.API_URL), ApiClient {

    override fun apiClass(): Class<Api> = Api::class.java

}