package ru.frosteye.checkregister.data.model

import com.google.gson.annotations.SerializedName

data class Attendee(
        @SerializedName("AttendeeUID") val uid: Int,
        @SerializedName("FirstName") val firstName: String,
        @SerializedName("LastName") val lastName: String,
        @SerializedName("Phone") val phone: String,
        @SerializedName("Amount") val amount: Int?,
        @SerializedName("AmountRemainder") val amountLeft: Int?,
        @SerializedName("CodesCnt") val codesCount: Int?,
        @SerializedName("Checks") val checks: List<Check>?
) : MainItem {
    override fun getTitle(): CharSequence = "$firstName $lastName"

    override fun getSubtitle(): CharSequence? = phone

    override fun getId(): Int = uid

}

data class Check(
        @SerializedName("ShopTitle") val title: String,
        @SerializedName("Amount") val amount: Int,
        @SerializedName("PurchaseCD") val purchaseCd: String
)

data class Shop(
        @SerializedName("ShopUID") val uid: Int,
        @SerializedName("Brand") val brand: String,
        @SerializedName("Title") val title: String
) : MainItem {
    override fun getTitle(): CharSequence = title

    override fun getSubtitle(): CharSequence? = brand

    override fun getId(): Int = uid
}

interface MainItem {
    fun getTitle(): CharSequence
    fun getSubtitle(): CharSequence?
    fun getId(): Int
}