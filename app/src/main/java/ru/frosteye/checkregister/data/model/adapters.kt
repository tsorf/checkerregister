package ru.frosteye.checkregister.data.model

import ru.frosteye.checkregister.R
import ru.frosteye.checkregister.presentation.view.impl.widget.CheckItemView
import ru.frosteye.ovsa.presentation.adapter.AdapterItem

class CheckAdapterItem(model: Check?) : AdapterItem<Check, CheckItemView>(model) {
    override fun getLayoutRes(): Int = R.layout.view_check_item

}