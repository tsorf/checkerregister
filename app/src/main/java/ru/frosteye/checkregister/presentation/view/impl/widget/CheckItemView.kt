package ru.frosteye.checkregister.presentation.view.impl.widget

import android.content.Context
import android.util.AttributeSet
import kotlinx.android.synthetic.main.view_check_item.view.*
import ru.frosteye.checkregister.R
import ru.frosteye.checkregister.data.model.Check
import ru.frosteye.ovsa.presentation.view.ModelView
import ru.frosteye.ovsa.presentation.view.widget.BaseConstraintLayout

class CheckItemView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : BaseConstraintLayout(context, attrs, defStyleAttr), ModelView<Check> {
    override var model: Check? = null
        set(value) {
            field = value
            value!!
            checkDate.text = value.purchaseCd
            checkAmount.text = resources.getString(R.string.pattern_amount, value.amount / 100f)
            checkShop.text = value.title
        }

    override fun prepareView() {

    }

}