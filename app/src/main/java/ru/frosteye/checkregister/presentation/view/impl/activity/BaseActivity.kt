package ru.frosteye.checkregister.presentation.view.impl.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.tbruyelle.rxpermissions2.RxPermissions
import ru.frosteye.checkregister.app.CheckerApp
import ru.frosteye.checkregister.app.di.PresenterComponent
import ru.frosteye.checkregister.app.di.PresenterModule
import ru.frosteye.ovsa.presentation.view.activity.PresenterActivity
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

abstract class BaseActivity : PresenterActivity() {

    protected lateinit var permissions: RxPermissions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        permissions = RxPermissions(this)
        val component = CheckerApp.appComponent + PresenterModule(this)
        component.inject(this)
        inject(component)
    }


    protected fun scanCode() {
        permissions.request(Manifest.permission.CAMERA)
                .subscribe {
                    if (it) {
                        startActivityForResult(Intent(this, ScannerActivity::class.java), 666)
                    }
                }
    }

    protected fun enableView(view: View, enabled: Boolean) {
        view.isEnabled = enabled
        if (enabled) {
            view.alpha = 1.0f
        } else {
            view.alpha = 0.6f
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 666 && resultCode == Activity.RESULT_OK) {
            val code = data?.getStringExtra("code")
            code?.let {
                onScannedCode(it)
            }
        }
    }

    abstract fun inject(component: PresenterComponent)

    abstract fun onScannedCode(code: String)
}