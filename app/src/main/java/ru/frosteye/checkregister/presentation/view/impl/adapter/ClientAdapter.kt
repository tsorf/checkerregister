package ru.frosteye.checkregister.presentation.view.impl.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import ru.frosteye.checkregister.R
import ru.frosteye.checkregister.app.CheckerApp
import ru.frosteye.checkregister.data.model.MainItem
import ru.frosteye.checkregister.execution.network.Api

class ClientAdapter(
        private val context: Context,
        private var items: MutableList<MainItem> = mutableListOf(),
        private val api: Api = CheckerApp.appComponent.api()
) : BaseAdapter(), Filterable {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var itemView = convertView
        var holder: ViewHolder? = null
        if (itemView == null) {
            itemView = LayoutInflater.from(context).inflate(R.layout.view_search_item, parent, false)
            holder = ViewHolder(itemView)
            itemView.tag = holder
        } else {
            holder = itemView.tag as ViewHolder
        }
        val item = getItem(position)
        holder.firstLine.text = item.getTitle()
        holder.secondLine.text = item.getSubtitle()
        return itemView!!
    }

    override fun getItem(position: Int): MainItem {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items.count()
    }

    override fun getFilter(): Filter {
        return object : Filter() {

            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val results = FilterResults()
                if (constraint != null) {
                    val query = constraint.toString().toLowerCase().trim()
                    val data = api.getAttendees(query).execute().body()!!
                    results.values = data.data
                    results.count = data.data.size
                }
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                var out = results
                if (out != null && out.count > 0) {
                    items = out.values as MutableList<MainItem>
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }
        }
    }

    inner class ViewHolder {

        val firstLine: TextView
        val secondLine: TextView

        constructor(view: View) {
            firstLine = view.findViewById<TextView>(android.R.id.text1)
            secondLine = view.findViewById<TextView>(android.R.id.text2)
        }
    }
}