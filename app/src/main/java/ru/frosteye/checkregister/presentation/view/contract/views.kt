package ru.frosteye.checkregister.presentation.view.contract

import ru.frosteye.checkregister.data.model.Attendee
import ru.frosteye.checkregister.data.model.MainItem
import ru.frosteye.checkregister.data.model.Shop
import ru.frosteye.ovsa.presentation.view.BasicView

interface MainView : BasicView {

    fun showFullAttendee(data: Attendee)
    fun showShop(it: Shop)
    fun processNext()
}