package ru.frosteye.checkregister.presentation.presenter

import io.reactivex.disposables.Disposable
import ru.frosteye.checkregister.R
import ru.frosteye.checkregister.data.model.Attendee
import ru.frosteye.checkregister.execution.network.Api
import ru.frosteye.checkregister.execution.network.apiRequest
import ru.frosteye.checkregister.presentation.view.contract.MainView
import ru.frosteye.ovsa.data.storage.ResourceHelper.getString
import ru.frosteye.ovsa.execution.network.request.RequestParams
import ru.frosteye.ovsa.execution.task.request
import ru.frosteye.ovsa.execution.task.toBack
import ru.frosteye.ovsa.presentation.presenter.BasePresenter
import ru.frosteye.ovsa.presentation.presenter.LivePresenter
import javax.inject.Inject

interface MainPresenter : LivePresenter<MainView> {
    fun findAttendeeById(attendee: Attendee)
    fun onShopSearch(shop: String)
    fun register(params: RequestParams, newRecord: Boolean)
    fun onResetPrint(attendee: Attendee)
}

class MainPresenterImpl @Inject constructor(
        private val api: Api
) : BasePresenter<MainView>(), MainPresenter {

    private var disposable: Disposable? = null


    override fun onResetPrint(attendee: Attendee) {
        enableControls(false)
        disposable?.dispose()
        api.resetPrint(attendeeId = attendee.getId())
                .toBack()
                .apiRequest({
                    enableControls(true)
                    showMessage(getString(R.string.success))
                }, {
                    enableControls(true)
                    showError(it.message)
                })
    }

    override fun register(params: RequestParams, newRecord: Boolean) {
        enableControls(false)
        disposable?.dispose()
        val call = if (newRecord) {
            api.addCheckWithAttendee(params)
        } else {
            api.addCheck(params)
        }
        disposable = call
                .toBack()
                .apiRequest({
                    enableControls(true)
                    view.showFullAttendee(it)
                    view.processNext()
                    showSuccess(getString(R.string.register_success))
                }, {
                    enableControls(true)
                    showError(it.message)
                })
    }

    override fun onShopSearch(shop: String) {
        enableControls(false)
        disposable?.dispose()
        disposable = api.getShop(shop)
                .toBack()
                .apiRequest({
                    enableControls(true)
                    view.showShop(it)
                }, {
                    enableControls(true)
                    showError(it.message)
                })
    }


    override fun findAttendeeById(attendee: Attendee) {
        enableControls(false)
        disposable?.dispose()
        disposable = api.getAttendee(attendee.getId())
                .toBack()
                .apiRequest({
                    enableControls(true)
                    view.showFullAttendee(it)
                }, {
                    enableControls(true)
                    showError(it.message)
                })
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }
}