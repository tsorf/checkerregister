package ru.frosteye.checkregister.presentation.view.impl.activity

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import eu.davidea.flexibleadapter.items.IFlexible
import kotlinx.android.synthetic.main.activity_main.*
import ru.frosteye.checkregister.R
import ru.frosteye.checkregister.app.di.PresenterComponent
import ru.frosteye.checkregister.data.model.Attendee
import ru.frosteye.checkregister.data.model.CheckAdapterItem
import ru.frosteye.checkregister.data.model.MainItem
import ru.frosteye.checkregister.data.model.Shop
import ru.frosteye.checkregister.presentation.presenter.MainPresenter
import ru.frosteye.checkregister.presentation.view.contract.MainView
import ru.frosteye.checkregister.presentation.view.impl.adapter.ClientAdapter
import ru.frosteye.checkregister.presentation.view.impl.adapter.ShopAdapter
import ru.frosteye.ovsa.execution.network.request.RequestParams
import ru.frosteye.ovsa.presentation.adapter.FlexibleModelAdapter
import ru.frosteye.ovsa.tool.DateTools
import ru.frosteye.ovsa.tool.isTrimmedEmpty
import ru.frosteye.ovsa.tool.trimmed
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainView {

    @Inject
    override lateinit var presenter: MainPresenter

    private lateinit var clientAdapter: ClientAdapter
    private lateinit var phoneAdapter: ClientAdapter
    private lateinit var shopAdapter: ShopAdapter
    private lateinit var checksAdapter: FlexibleModelAdapter<IFlexible<*>>

    private var lastEdited: EditText? = null
    private var isWatchersBlocked = false

    private var shop: Shop? = null
    private var attendee: Attendee? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun initView(savedInstanceState: Bundle?) {

        checksAdapter = FlexibleModelAdapter(listOf())
        mainCheckList.layoutManager = LinearLayoutManager(this)
        mainCheckList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        mainCheckList.adapter = checksAdapter

        clientAdapter = ClientAdapter(this)
        phoneAdapter = ClientAdapter(this)
        shopAdapter = ShopAdapter(this)

        mainLastName.setAdapter(clientAdapter)
        mainLastName.setOnItemClickListener { parent, view, position, id ->
            val item = parent.getItemAtPosition(position) as Attendee
            presenter.findAttendeeById(item)
        }

        mainPhone.setAdapter(phoneAdapter)
        mainPhone.setOnItemClickListener { parent, view, position, id ->
            val item = parent.getItemAtPosition(position) as Attendee
            presenter.findAttendeeById(item)
        }
        mainPrintCoupons.setOnClickListener {
            attendee?.let {at -> presenter.onResetPrint(at) }
        }

        mainShop.setAdapter(shopAdapter)
        mainShop.setOnItemClickListener { parent, view, position, id ->
            val item = parent.getItemAtPosition(position) as Shop
            showFullShop(item)
        }

        mainNext.setOnClickListener { clearAll() }
        mainScan.setOnClickListener { scanCode() }

        mainRegister.setOnClickListener { registerCheck() }
        mainDate.setText(DateTools.formatDottedDate(Date()))
        mainPrintCoupons

        registerTextChangeListeners({
            validatePrint()
            if (isWatchersBlocked) return@registerTextChangeListeners
            checkAndClean(it)
            mainRegister.isEnabled = validate()
        }, mainName, mainLastName, mainPhone, mainDate, mainSum, mainShop)
        mainDate.setOnTouchListener { v, event ->
            if (event.action != MotionEvent.ACTION_UP) return@setOnTouchListener false
            DateTools.showDateDialogWithButtons(this, object : DateTools.DatePickerCallback {
                override fun onDateSelected(date: Date) {
                    mainDate.setText(DateTools.formatDottedDate(date))
                }

            }, Calendar.getInstance(), false)
            true
        }
    }

    private fun validatePrint() {
        mainPrintCoupons.isEnabled = attendee != null
                && attendee!!.phone == "7${mainPhone.trimmed()}"

    }

    private fun showFullShop(item: Shop) {
        this.shop = item
        setFocus(false, mainShop)
        mainShop.setText(item.title)
        setFocus(true, mainShop)
    }

    private fun registerCheck() {
        val params = RequestParams()
        params.addParam("amount", (mainSum.trimmed().toFloat() * 100).toInt())
        params.addParam("purchasecd", mainDate.trimmed())
        if (shop != null) {
            params.addParam("shopuid", shop!!.uid)
        } else {
            params.addParam("shopuid", mainShop.trimmed())
        }
        if (attendee == null
                || attendee!!.phone != "7${mainPhone.trimmed()}"
        ) {
            params.addParam("firstname", mainName.trimmed())
            params.addParam("lastname", mainLastName.trimmed())
            params.addParam("phone", "7${mainPhone.trimmed()}")
        } else {
            params.addParam("attendeeuid", attendee!!.uid)
        }
        presenter.register(params, attendee == null)
    }

    private fun checkAndClean(it: Editable) {
        val input = when {
            it === mainPhone.text -> mainPhone
            it === mainLastName.text -> mainLastName
            it === mainName.text -> mainName
            it === mainShop.text -> {

                null
            }
            else -> null
        }
        if (input != null) {
            lastEdited = input
        }

    }

    private fun validate(): Boolean {
        return checkUser()
                && !mainSum.isTrimmedEmpty()
                && !mainDate.isTrimmedEmpty()
                && shop != null
    }

    private fun checkUser(): Boolean {
        return attendee != null ||
                (!mainName.isTrimmedEmpty()
                        && !mainLastName.isTrimmedEmpty()
                        && !mainPhone.isTrimmedEmpty()
                        && mainPhone.trimmed().length == 10)
    }

    private fun clearAll() {
        isWatchersBlocked = true
        shop = null
        attendee = null
        validatePrint()
        checksAdapter.clear()
        mainPhone.text = null
        mainLastName.text = null
        mainName.text = null
        mainDate.setText(DateTools.formatDottedDate(Date()))
        mainSum.text = null
        mainShop.text = null
        mainPrice.text = null
        mainCoupons.text = null
        isWatchersBlocked = false
    }

    override fun processNext() {
        isWatchersBlocked = true
        mainSum.text = null
        isWatchersBlocked = false
        mainRegister.isEnabled = false
    }

    private fun setFocus(enabled: Boolean, vararg items: EditText) {
        items.forEach {
            it.isFocusable = enabled
            it.isFocusableInTouchMode = enabled
        }
    }

    override fun showFullAttendee(data: Attendee) {
        this.attendee = data
        setFocus(false, mainLastName, mainPhone)
        checksAdapter.updateDataSet(data.checks?.map { CheckAdapterItem(it) })
        mainName.setText(data.firstName)
        mainLastName.setText(data.lastName)
        var phone = data.phone
        if (phone.startsWith("7")) {
            phone = phone.substring(1)
        }
        mainPhone.setText(phone)
        mainCoupons.setText(data.codesCount.toString())
        setFocus(true, mainLastName, mainPhone)

        val left = data.amountLeft?.let {
            it / 100f
        } ?: 0f
        mainPrice.text = getString(R.string.pattern_amount, left)
        validatePrint()
    }

    private fun processSearch(mainItem: MainItem) {
        if (mainItem is Attendee) {
            presenter.findAttendeeById(mainItem)
        }
    }

    override fun enableControls(enabled: Boolean, code: Int) {
        super.enableControls(enabled, code)
        enableView(mainName, enabled)
        enableView(mainLastName, enabled)
        enableView(mainPhone, enabled)
        enableView(mainShop, enabled)
        enableView(mainSum, enabled)
        enableView(mainDate, enabled)
        mainScan.isEnabled = enabled
        mainRegister.isEnabled = validate()
        mainPrice.isEnabled = enabled
        mainProgress.visibility = if (enabled) View.INVISIBLE else View.VISIBLE
        validatePrint()
    }

    override fun inject(component: PresenterComponent) {
        component.inject(this)
    }

    override fun attachPresenter() {
        presenter.onAttach(this)
    }

    override fun showShop(it: Shop) {
        this.shop = it
        setFocus(false, mainShop)
        mainShop.setText(it.getTitle())
        setFocus(true, mainShop)
    }

    override fun onScannedCode(code: String) {
        val parts = code.split("&")
        var shop: String? = null
        parts.forEach {
            val param = it.split("=")
            when (param[0]) {
                "s" -> mainSum.setText(param[1])
                "fn" -> {
                    shop = param[1]
                }
                "t" -> setDate(param[1])
            }
        }
        if (shop == null) {
            showMessage(getString(R.string.shop_not_found))
            return
        } else {
            presenter.onShopSearch(shop!!)
        }
    }

    private fun setDate(t: String) {
        val date = t.split("T")[0]
        val formatted = SimpleDateFormat("yyyyMMdd", Locale.getDefault())
                .parse(date)
        mainDate.setText(DateTools.formatDottedDate(formatted))
    }
}
